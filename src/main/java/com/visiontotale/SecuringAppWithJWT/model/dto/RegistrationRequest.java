package com.visiontotale.SecuringAppWithJWT.model.dto;

import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Class that holds information regarding the registration of a user. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequest {
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private Set<String> profils = new HashSet<>();
}
