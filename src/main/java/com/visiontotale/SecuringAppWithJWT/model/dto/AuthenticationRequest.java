package com.visiontotale.SecuringAppWithJWT.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Class that holds information regarding the authentication request of a user. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest {
  private String email;
  private String password;
}
