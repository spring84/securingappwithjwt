package com.visiontotale.SecuringAppWithJWT.model.authentication;

import java.util.Map;
import java.util.Set;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/** Interface that specifies the usefully fields of a user. */
public interface TokenizableUser extends UserDetails {

  /**
   * First name of the user.
   *
   * @return the first name of the user
   */
  String getFirstName();

  /**
   * Last name of the user.
   *
   * @return the last name of the user
   */
  String getLastName();

  /**
   * Email of the user.
   *
   * @return the full e-mail of the user
   */
  String getEmail();

  /**
   * Password of the user.
   *
   * @return the full password of the user
   */
  String getPassword();

  /**
   * List of profils of the user.
   *
   * @return the list of profils of the user
   */
  Set<String> getProfils();

  /**
   * Full name of the user.
   *
   * @return the full name of the user
   */
  String getFullName();

  /**
   * The Authentication object.
   *
   * @return the authentication object
   */
  Authentication authentication();

  /**
   * Claims.
   *
   * @return the claims
   */
  Map<String, Object> getClaims();
}
