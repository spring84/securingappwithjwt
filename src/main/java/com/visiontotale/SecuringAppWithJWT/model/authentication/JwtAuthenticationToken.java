package com.visiontotale.SecuringAppWithJWT.model.authentication;

import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/** Class that implements an authentication token including a tokenizable user in the jwt. */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {
  public JwtAuthenticationToken(User user, Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    setDetails(user);
  }

  /**
   * The connexion credentials. Useless in this case.
   *
   * @return the UserDetails.
   */
  @Override
  public Object getCredentials() {
    return "";
  }

  /**
   * The principal that corresponds to the UserDetails.
   *
   * @return the UserDetails.
   */
  @Override
  public Object getPrincipal() {
    return getDetails();
  }
}
