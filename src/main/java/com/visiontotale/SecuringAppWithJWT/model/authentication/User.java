package com.visiontotale.SecuringAppWithJWT.model.authentication;

import com.visiontotale.SecuringAppWithJWT.enums.UserAttribute;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/** Class for manipulating the User data. */
@Data
@Builder
public class User implements TokenizableUser {

  private Map<String, Object> claims;
  private Collection<? extends GrantedAuthority> authorities;

  /**
   * This method allows to retrieve an information stored in claims.
   *
   * @param name the name of the parameter
   * @param <T> the type of parameter to be retrieved
   * @return the first name of the user
   */
  protected <T> T getClaimValue(String name) {
    return (T) getClaims().getOrDefault(name, null);
  }

  @Override
  public String getFirstName() {
    return getClaimValue(UserAttribute.FIRST_NAME.name());
  }

  @Override
  public String getLastName() {
    return getClaimValue(UserAttribute.LAST_NAME.name());
  }

  @Override
  public String getEmail() {
    return getClaimValue(UserAttribute.EMAIL.name());
  }

  @Override
  public String getPassword() {
    return "****************";
  }

  @Override
  public String getUsername() {
    return getClaimValue(UserAttribute.EMAIL.name());
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public Set<String> getProfils() {
    return getClaimValue(UserAttribute.PROFILS.name());
  }

  @Override
  public String getFullName() {
    return getClaimValue(UserAttribute.FULL_NAME.name());
  }

  @Override
  public Authentication authentication() {
    return new JwtAuthenticationToken(this, getAuthorities());
  }
}
