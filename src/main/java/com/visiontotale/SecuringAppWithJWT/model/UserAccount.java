package com.visiontotale.SecuringAppWithJWT.model;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

/**
 * Class that represents the user account and which will be used by the validator to validate the
 * user account during authentication.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAccount {
  /** The username of the user which in our case is the email. */
  @Builder.Default @NotBlank private String email = "";

  /** Password to be tested during authentication. */
  @Builder.Default @NotBlank private String password = "";
}
