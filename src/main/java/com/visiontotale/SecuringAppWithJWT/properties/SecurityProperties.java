package com.visiontotale.SecuringAppWithJWT.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Global configuration properties */
@Getter
@Setter
@Component
@ConfigurationProperties
public class SecurityProperties {

  /** Is security enabled? */
  private boolean enabled;

  /** Is CORS configuration enabled? */
  private boolean corsWildCardEnabled;

  /** List of URLs that will not be subject to security constraints */
  private String[] whiteUrlsList;

  /** List of urls that must go through token */
  private String[] securedUlrsList;
}
