package com.visiontotale.SecuringAppWithJWT.config;

import com.visiontotale.SecuringAppWithJWT.provider.AuthenticationProvider;
import com.visiontotale.SecuringAppWithJWT.provider.AuthenticationProviderImpl;
import com.visiontotale.SecuringAppWithJWT.provider.UserInformationProvider;
import com.visiontotale.SecuringAppWithJWT.provider.UserInformationProviderImpl;
import com.visiontotale.SecuringAppWithJWT.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Class that defines the beans related to the user. */
@Configuration
public class UserConfig {

  /**
   * To be redefined for a use according to the desired authentication logic that we wish to put in
   * place.
   *
   * @param userRepository the user repository that allows to interact with the user data in the
   *     database.
   * @return the authentication provider.
   */
  @Bean
  public AuthenticationProvider authenticationProvider(UserRepository userRepository) {
    return new AuthenticationProviderImpl(userRepository);
  }

  /**
   * To be redefined for a normal use of the user management service that we wish to put in place.
   *
   * @return the user's business information provider.
   */
  @Bean
  public UserInformationProvider userInformationProvider(UserRepository userRepository) {
    return new UserInformationProviderImpl(userRepository);
  }
}
