package com.visiontotale.SecuringAppWithJWT.config;

import com.visiontotale.SecuringAppWithJWT.converter.Converter;
import com.visiontotale.SecuringAppWithJWT.converter.JwtToUserConverter;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.service.jwt.JwtService;
import com.visiontotale.SecuringAppWithJWT.service.jwt.TokenStore;
import com.visiontotale.SecuringAppWithJWT.service.jwt.header.HeaderBearerTokenStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class that allows to configure everything related to the authentication management
 * token.
 */
@Configuration
public class TokenConfig {

  /**
   * To be redefined for a normal use of the user management service that we wish to put in place.
   *
   * @return the header Bearer Token Store.
   */
  @Bean
  public TokenStore headerBearerTokenStore() {
    return new HeaderBearerTokenStore();
  }

  /**
   * To be redefined for a normal use of the user management service that we wish to put in place.
   *
   * @return the jwt ToUser Converter.
   */
  @Bean
  public Converter<String, TokenizableUser> jwtToUserConverter() {
    return new JwtToUserConverter(jwtService());
  }

  private JwtService jwtService() {
    return new JwtService(headerBearerTokenStore());
  }
}
