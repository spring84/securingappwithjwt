package com.visiontotale.SecuringAppWithJWT.config;

import com.visiontotale.SecuringAppWithJWT.converter.Converter;
import com.visiontotale.SecuringAppWithJWT.converter.RegistrationRequestToUserConverter;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.dto.RegistrationRequest;
import com.visiontotale.SecuringAppWithJWT.properties.SecurityProperties;
import jakarta.validation.Valid;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * Security Configuration Class. A class tels spring which configuration we want to use in order to
 * make the whole security work.
 */
@Configuration
@EnableWebSecurity
@AllArgsConstructor
@Slf4j
public class SecurityConfig {

  /** Access to the token authentication filter */
  private final JwtAuthenticationFilter jwtAuthFilter;

  /** Access to specific security properties */
  @Valid private final SecurityProperties security;

  /**
   * Configuring HTTP Security.
   *
   * @param http Access to the configuration of the HTTP part of HTTP security.
   * @throws Exception generic exception thrown during configuration.
   * @return the Security Filter Chain.
   */
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

    // Disabling the page cache
    http.headers().cacheControl().disable();

    http.csrf()
        .disable()
        .authorizeHttpRequests()
        .requestMatchers(getWhiteListEndPoints())
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

    return http.build();
  }

  /**
   * Gets the list of urls to be secured by the token. (Not used for now because for the moment,
   * apart from the whitelisted urls, all other urls are secured by the token. But this will change
   * soon.)
   *
   * @return the list of urls to be secured by the token.
   */
  private RequestMatcher[] getSecuredUrlsList() {
    return security.getSecuredUlrsList() == null
        ? new AntPathRequestMatcher[] {new AntPathRequestMatcher("/**")}
        : Arrays.stream(security.getSecuredUlrsList())
            .map(AntPathRequestMatcher::new)
            .toArray(RequestMatcher[]::new);
  }

  /**
   * Gets the list of URLs that will not be subject to security constraints.
   *
   * @return the list of urls.
   */
  private RequestMatcher[] getWhiteListEndPoints() {
    if (ArrayUtils.isNotEmpty(security.getWhiteUrlsList())) {
      return Arrays.stream(security.getWhiteUrlsList())
          .map(AntPathRequestMatcher::new)
          .toArray(RequestMatcher[]::new);
    } else {
      log.info("Using the default white list");
      return new RequestMatcher[] {
        new AntPathRequestMatcher("/api/auth/**"),
        new AntPathRequestMatcher("/v3/api-docs/**"),
        new AntPathRequestMatcher("configuration/**"),
        new AntPathRequestMatcher("/swagger*/**"),
        new AntPathRequestMatcher("/webjars/**"),
        new AntPathRequestMatcher("/swagger-ui/**"),
        new AntPathRequestMatcher("swagger-ui.html")
      };
    }
  }

  @Bean
  public Converter<RegistrationRequest, TokenizableUser> registratioRrequestToUserConverter() {
    return new RegistrationRequestToUserConverter();
  }

  /**
   * Bean that returns the password encoder that we are using for the application We need to be
   * aware of it in order to be able to decode the password using the correct algorithm.
   *
   * @return the password encoder.
   */
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
