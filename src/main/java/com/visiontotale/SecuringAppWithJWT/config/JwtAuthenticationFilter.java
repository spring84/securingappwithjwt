package com.visiontotale.SecuringAppWithJWT.config;

import com.visiontotale.SecuringAppWithJWT.converter.Converter;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.service.jwt.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Authentication Filter, which will be the first thing that intercepts all http requests. We want
 * it to be fired each time a request comes in. That's the reason why we choose to extend
 * OncePerRequestFilter.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

  private final JwtService jwtService;
  private final Converter<String, TokenizableUser> jwtToUserConverter;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    final String jwt = jwtService.getToken(request);

    if (jwt != null) {
      log.debug("Jwt Authentication: jwt found");
      Authentication authentication = jwtToUserConverter.convert(jwt).authentication();
      if (authentication != null) {
        log.debug("Jwt Authentication: authentication {} found", authentication.getName());
        // Update the security context holder
        SecurityContextHolder.getContext().setAuthentication(authentication);
        authentication.setAuthenticated(true);
      } else {
        log.debug("Jwt Authentication: no authentication");
      }
    } else {
      log.debug("Jwt Authentication: no authentication");
    }

    // Pass the hand to the next filter to be executed.
    filterChain.doFilter(request, response);
  }
}
