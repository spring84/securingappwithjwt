package com.visiontotale.SecuringAppWithJWT.provider;

import com.visiontotale.SecuringAppWithJWT.model.UserAccount;
import com.visiontotale.SecuringAppWithJWT.model.entity.UserEntity;
import com.visiontotale.SecuringAppWithJWT.repository.UserRepository;
import com.visiontotale.SecuringAppWithJWT.validator.UserAccountAuthenticationValidator;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

/** Customized authentication provider. */
@RequiredArgsConstructor
public class AuthenticationProviderImpl implements AuthenticationProvider {

  private final UserRepository userRepository;

  @Override
  public boolean authenticate(String userName, String password) {
    Optional<UserEntity> userEntity = userRepository.findByEmail(userName);

    if (userEntity.isPresent()) {
      UserAccount userAccount =
          UserAccount.builder()
              .email(userEntity.get().getEmail())
              .password(userEntity.get().getPassword())
              .build();

      new UserAccountAuthenticationValidator(userAccount).isValidOrThrow();

      return Boolean.TRUE;
    } else {
      return Boolean.FALSE;
    }
  }
}
