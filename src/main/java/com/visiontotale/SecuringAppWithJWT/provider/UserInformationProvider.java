package com.visiontotale.SecuringAppWithJWT.provider;

import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;

/**
 * This interface specifies a user information provider. The main method is used to enrich a user
 * (according to the security module) with its business information. Information can come from
 * external modules (to be defined), or internal classes (for instance for testing)
 */
@FunctionalInterface
public interface UserInformationProvider {

  /**
   * This method allows to enrich a user with functional user information.
   *
   * @param user the user to enrich
   * @return the enriched user
   */
  TokenizableUser enrich(final TokenizableUser user);
}
