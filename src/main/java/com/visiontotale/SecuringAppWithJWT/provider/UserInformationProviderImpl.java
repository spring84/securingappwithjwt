package com.visiontotale.SecuringAppWithJWT.provider;

import com.visiontotale.SecuringAppWithJWT.enums.UserAttribute;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.authentication.User;
import com.visiontotale.SecuringAppWithJWT.model.entity.UserEntity;
import com.visiontotale.SecuringAppWithJWT.repository.UserRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** This class allows to create a user with customized information */
@RequiredArgsConstructor
@Slf4j
public class UserInformationProviderImpl implements UserInformationProvider {

  private final UserRepository userRepository;

  /**
   * An implementation of user enrichment consists of enriching his claims with customized
   * information such as his profiles, his rights, the functionalities he has access to, his
   * authorities, ...
   *
   * <p>For now, we'll just concentrate on his profiles.
   *
   * @param user the secured user to enrich.
   * @return the new user along with all its required claims.
   */
  @Override
  public TokenizableUser enrich(TokenizableUser user) {

    Map<String, Object> newClaims = new HashMap<>(user.getClaims());

    Optional<UserEntity> userEntity = userRepository.findByEmail(user.getEmail());

    if (userEntity.isPresent()) {
      Set<String> profils = userRepository.findProfilsByUserId(userEntity.get().getId());
      newClaims.put(UserAttribute.PROFILS.name(), profils);
    }

    return User.builder().claims(newClaims).build();
  }
}
