package com.visiontotale.SecuringAppWithJWT.provider;

/**
 * Interface that defines a provider (not related to spring security). It allows to customize user
 * authentication
 */
public interface AuthenticationProvider {
  boolean authenticate(String userName, String password);
}
