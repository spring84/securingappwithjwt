package com.visiontotale.SecuringAppWithJWT.validator;

import com.visiontotale.SecuringAppWithJWT.exception.ValidationException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DirectFieldBindingResult;

@Getter
@Slf4j
public abstract class AbstractValidator<T> implements Validator<T> {

  private final T target;
  private final String name;
  private final BindingResult result;
  private boolean validated;

  protected AbstractValidator(T target, String name) {
    this.target = target;
    this.name = name;
    this.result = new DirectFieldBindingResult(target, name);
  }

  public boolean isValid() {
    validateOneTime();
    return !getResult().hasErrors();
  }

  protected abstract void validate();

  private void validateOneTime() {
    if (!validated) {
      validated = true;
      validate();
    }
  }

  public boolean isValidOrThrow() {
    validateOneTime();
    if (!isValid()) {
      if (log.isDebugEnabled()) {
        logErrors();
      }
      throw new ValidationException(
          String.format("%s validation error(s).", getResult().getErrorCount()), getResult());
    }
    return true;
  }

  public BindingResult getResult() {
    validateOneTime();
    return result;
  }

  private void logErrors() {
    log.debug(
        "Number of errors : {} global(s), {} on the fields",
        getResult().getGlobalErrorCount(),
        getResult().getFieldErrorCount());

    if (getResult().getGlobalErrorCount() > 0) {
      log.debug("Global errors :");
      getResult().getGlobalErrors().forEach(f -> log.debug(f.getDefaultMessage()));
    }

    if (getResult().getFieldErrorCount() > 0) {
      log.debug("Errors on fields :");
      getResult()
          .getFieldErrors()
          .forEach(f -> log.debug("{} : {} ", f.getField(), f.getDefaultMessage()));
    }
  }
}
