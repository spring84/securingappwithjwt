package com.visiontotale.SecuringAppWithJWT.validator;

import org.springframework.validation.BindingResult;

/**
 * Interface on which will rely the functional validation mechanism. Mechanism which will in its
 * entirety be based on the interface {@link BindingResult}
 *
 * @param <T> the type of the object to be validated.
 */
public interface Validator<T> {

  /**
   * Returns {@code true} if there is no error.
   *
   * @return {@code true} if there is no error.
   */
  boolean isValid();

  /**
   * Returns {@code true} if there is no error or throws exception {@link ValidationException}
   * otherwise.
   *
   * @return {@code true} if there is no error.
   */
  boolean isValidOrThrow();

  /**
   * Returns the list of results following validation.
   *
   * @return the list of results following validation.
   */
  BindingResult getResult();

  /**
   * Returns the functional name of the object to be validated.
   *
   * @return the functional name of the object to be validated.
   */
  String getName();

  /**
   * Returns the targeted object to be validated.
   *
   * @return the targeted object to be validated.
   */
  T getTarget();
}
