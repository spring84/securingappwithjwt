package com.visiontotale.SecuringAppWithJWT.validator;

import com.visiontotale.SecuringAppWithJWT.model.UserAccount;

/** This validator allows to customize the validation of the user account during authentication. */
public class UserAccountAuthenticationValidator extends AbstractValidator<UserAccount> {

  public UserAccountAuthenticationValidator(UserAccount userAccount) {
    super(userAccount, "Authentication");
  }

  @Override
  protected void validate() {
    /**
     * Right now we are not doing any validation action on the user account. But soon we are going
     * to get into it. This will include validating the technical account of the user, validating
     * the password, the identifier, checking whether or not the user's account is still active,
     * checking whether or not the user's password has expired, ...
     */
  }
}
