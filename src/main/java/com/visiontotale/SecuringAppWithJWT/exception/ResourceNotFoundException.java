package com.visiontotale.SecuringAppWithJWT.exception;

import com.visiontotale.SecuringAppWithJWT.enums.InterModuleError;
import lombok.Getter;
import lombok.NoArgsConstructor;

/** Class of exceptions triggered when a Total Vision resource cannot be found. */
@Getter
@NoArgsConstructor
public class ResourceNotFoundException extends AbstractVisionTotaleException {

  /**
   * Constructor with message.
   *
   * @param message the error message.
   */
  public ResourceNotFoundException(String message) {
    super(message, InterModuleError.AUCUN_RESULTAT.getCode());
  }

  /**
   * Constructor with message + functional code.
   *
   * @param message the error message.
   * @param code functional code of the error.
   */
  public ResourceNotFoundException(String message, String code) {
    super(message, code);
  }

  /**
   * Constructor + message + cause allowing to catch and re-throw an exception + functional code.
   *
   * @param message the error message.
   * @param cause the cause of the error.
   */
  public ResourceNotFoundException(String message, Throwable cause) {
    super(message, cause, InterModuleError.AUCUN_RESULTAT.getCode());
  }

  /**
   * Constructor + message + cause allowing to catch and re-throw an exception.
   *
   * @param message the error message.
   * @param cause the cause of the error.
   * @param code the functional code of the error.
   */
  public ResourceNotFoundException(String message, Throwable cause, String code) {
    super(message, cause, code);
  }
}
