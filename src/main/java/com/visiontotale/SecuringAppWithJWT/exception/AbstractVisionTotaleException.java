package com.visiontotale.SecuringAppWithJWT.exception;

import com.visiontotale.SecuringAppWithJWT.enums.InterModuleError;
import lombok.Getter;

/** Abstract class of exceptions triggered when a Vision Totale resource is not found. */
@Getter
public abstract class AbstractVisionTotaleException extends RuntimeException {
  /** The error functional code */
  private final String code;

  /** No-argument constructor */
  public AbstractVisionTotaleException() {
    super();
    this.code = InterModuleError.ERREUR_INCONNUE.getCode();
  }

  /**
   * Constructor with message.
   *
   * @param message the error message.
   */
  protected AbstractVisionTotaleException(String message) {
    super(message);
    this.code = InterModuleError.ERREUR_INCONNUE.getCode();
  }

  /**
   * Constructor with message + functional code.
   *
   * @param message the error message.
   * @param code functional code.
   */
  protected AbstractVisionTotaleException(String message, String code) {
    super(message);
    this.code = code;
  }

  /**
   * Constructor + message + cause to catch and re-throw an exception
   *
   * @param message the error message.
   * @param cause the cause of error.
   */
  protected AbstractVisionTotaleException(String message, Throwable cause) {
    super(message, cause);
    this.code = InterModuleError.ERREUR_INCONNUE.getCode();
  }

  /**
   * Constructor + message + cause allowing to catch and raise an exception + functional code.
   *
   * @param message the error message.
   * @param cause the cause of error.
   * @param code functional code.
   */
  protected AbstractVisionTotaleException(String message, Throwable cause, String code) {
    super(message, cause);
    this.code = code;
  }
}
