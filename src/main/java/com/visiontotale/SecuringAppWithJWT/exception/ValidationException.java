package com.visiontotale.SecuringAppWithJWT.exception;

import com.visiontotale.SecuringAppWithJWT.enums.InterModuleError;
import lombok.Getter;
import org.springframework.validation.BindingResult;

/** Exception thrown on business logic validation error. */
@Getter
public class ValidationException extends AbstractVisionTotaleException {

  private final BindingResult errors;

  /**
   * Exception constructor with message and error.
   *
   * @param message the basic message of the exception.
   * @param errors the list of results (errors?).
   */
  public ValidationException(String message, BindingResult errors) {
    super(message, InterModuleError.ERREUR_AUTORISATION.getCode());
    this.errors = errors;
  }

  /**
   * Exception constructor with message, error and functional code.
   *
   * @param message the base message of the exception.
   * @param errors the list of results (errors?).
   * @param code functional code.
   */
  public ValidationException(String message, BindingResult errors, String code) {
    super(message, code);
    this.errors = errors;
  }
}
