package com.visiontotale.SecuringAppWithJWT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuringAppWithJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuringAppWithJwtApplication.class, args);
	}

}
