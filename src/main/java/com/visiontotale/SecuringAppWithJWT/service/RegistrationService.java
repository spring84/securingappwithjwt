package com.visiontotale.SecuringAppWithJWT.service;

import com.visiontotale.SecuringAppWithJWT.converter.Converter;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.dto.RegistrationRequest;
import com.visiontotale.SecuringAppWithJWT.model.entity.UserEntity;
import com.visiontotale.SecuringAppWithJWT.repository.UserRepository;
import com.visiontotale.SecuringAppWithJWT.service.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/** Class that represents the service that is used to register a new user. */
@Service
@RequiredArgsConstructor
public class RegistrationService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtService jwtService;
  private final Converter<RegistrationRequest, TokenizableUser> registratioRrequestToUserConverter;

  /**
   * Method called to register a new user.
   *
   * @param request the registration request which contains the information of the user who wants to
   *     register.
   * @return the token of the new user.
   */
  public String register(RegistrationRequest request) {

    UserEntity userEntity =
        UserEntity.builder()
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .email(request.getEmail())
            .fullName(request.getFirstName() + " " + request.getLastName())
            .password(passwordEncoder.encode(request.getPassword()))
            .profils(request.getProfils())
            .build();

    userRepository.save(userEntity);

    TokenizableUser user =
        (TokenizableUser)
            registratioRrequestToUserConverter.convert(request).authentication().getPrincipal();

    return jwtService.generateToken(user.getClaims(), user);
  }
}
