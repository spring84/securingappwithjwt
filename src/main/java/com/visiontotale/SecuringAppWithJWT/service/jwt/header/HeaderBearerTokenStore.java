package com.visiontotale.SecuringAppWithJWT.service.jwt.header;

import com.visiontotale.SecuringAppWithJWT.service.jwt.TokenStore;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Class that allows to store the token in the header of the HTTP request/response and to later get
 * it from there for upcoming authentication.
 */
public class HeaderBearerTokenStore implements TokenStore {

  /** The name of the HTTP request/response header. */
  protected static final String HEADER_KEY = "Authorization";

  /** The value of the header, which will hold the token. */
  protected static final String HEADER_VALUE_PREFIX = "Bearer ";

  @Override
  public void store(final String token, final HttpServletResponse response, int validity) {
    store(token, response);
  }

  @Override
  public void store(final String token, final HttpServletResponse response) {
    if (token == null) {
      return;
    }

    if (token.isEmpty()) {
      return;
    }

    response.addHeader(HEADER_KEY, HEADER_VALUE_PREFIX + token);
  }

  @Override
  public String get(final HttpServletRequest request) {
    if (request.getHeader(HEADER_KEY) == null) {
      return "";
    }

    if (!request.getHeader(HEADER_KEY).startsWith(HEADER_VALUE_PREFIX)) {
      return "";
    }

    return request.getHeader(HEADER_KEY).replaceFirst(HEADER_VALUE_PREFIX, "");
  }

  @Override
  public void delete(HttpServletRequest request, HttpServletResponse response) {
    response.getHeaders(HEADER_KEY).clear();
  }
}
