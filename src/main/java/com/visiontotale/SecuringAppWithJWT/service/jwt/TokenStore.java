package com.visiontotale.SecuringAppWithJWT.service.jwt;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/** Interface that defines the management of the token storage. */
public interface TokenStore {

  /**
   * Stores the token in the HTTP response, indicating the validity period.
   *
   * @param token the token.
   * @param response the token.
   * @param validity the validity period of the token.
   */
  void store(String token, HttpServletResponse response, int validity);

  /**
   * Stores the token in the HTTP response.
   *
   * @param token the token.
   * @param response the token.
   */
  void store(String token, HttpServletResponse response);

  /**
   * Gets the token from the HTTP Servlet request.
   *
   * @param request the HTTP request.
   * @return the token.
   */
  String get(HttpServletRequest request);

  /**
   * Deletes the token from the HTTP Servlet response.
   *
   * @param request the HTTP request.
   * @param response the HTTP response.
   */
  void delete(HttpServletRequest request, HttpServletResponse response);
}
