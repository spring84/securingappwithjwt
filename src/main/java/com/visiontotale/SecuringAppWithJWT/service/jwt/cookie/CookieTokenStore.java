package com.visiontotale.SecuringAppWithJWT.service.jwt.cookie;

import com.visiontotale.SecuringAppWithJWT.service.jwt.TokenStore;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Class that allows to store the token in the cookie of the HTTP request/response and to later get
 * it from there for upcoming authentication.
 */
public class CookieTokenStore implements TokenStore {
  @Override
  public void store(String token, HttpServletResponse response, int validity) {}

  @Override
  public void store(String token, HttpServletResponse response) {}

  @Override
  public String get(HttpServletRequest request) {
    return null;
  }

  @Override
  public void delete(HttpServletRequest request, HttpServletResponse response) {}
}
