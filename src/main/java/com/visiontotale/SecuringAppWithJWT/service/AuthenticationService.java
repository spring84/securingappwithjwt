package com.visiontotale.SecuringAppWithJWT.service;

import com.visiontotale.SecuringAppWithJWT.enums.ErrorMessage;
import com.visiontotale.SecuringAppWithJWT.enums.UserAttribute;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.authentication.User;
import com.visiontotale.SecuringAppWithJWT.model.dto.AuthenticationRequest;
import com.visiontotale.SecuringAppWithJWT.provider.AuthenticationProvider;
import com.visiontotale.SecuringAppWithJWT.provider.UserInformationProvider;
import java.util.Collections;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@Slf4j
public class AuthenticationService implements UserDetailsService {

  private final AuthenticationProvider authenticationProvider;
  private final UserInformationProvider userInformationProvider;

  public AuthenticationService(
      AuthenticationProvider authenticationProvider,
      UserInformationProvider userInformationProvider) {
    Assert.notNull(authenticationProvider, "A user authentication provider must be defined.");
    Assert.notNull(userInformationProvider, "A user information provider must be defined");
    this.authenticationProvider = authenticationProvider;
    this.userInformationProvider = userInformationProvider;
  }

  /**
   * Method that allows to authenticate a user. The authentication token that is returned can then
   * be stored in designated token store (cookie or session)
   *
   * @param request the authentication request (email and password).
   * @return the authentication token.
   */
  public Authentication authenticate(AuthenticationRequest request) {
    if (authenticationProvider.authenticate(request.getEmail(), request.getPassword())) {
      TokenizableUser user = (TokenizableUser) loadUserByUsername(request.getEmail());
      return user.authentication();
    } else {
      throw new BadCredentialsException(
          String.format(ErrorMessage.BAD_CREDENTIALS.getMessage(), request.getEmail()));
    }
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user =
        User.builder()
            .claims(Map.of(UserAttribute.EMAIL.name(), username))
            .authorities(Collections.emptyList())
            .build();
    SecurityContextHolder.getContext().setAuthentication(user.authentication());
    return userInformationProvider.enrich(user);
  }
}
