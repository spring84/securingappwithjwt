package com.visiontotale.SecuringAppWithJWT.controller;

import com.visiontotale.SecuringAppWithJWT.model.dto.AuthenticationRequest;
import com.visiontotale.SecuringAppWithJWT.model.dto.RegistrationRequest;
import com.visiontotale.SecuringAppWithJWT.service.AuthenticationService;
import com.visiontotale.SecuringAppWithJWT.service.RegistrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Registration and authentication Controller. */
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Tag(name = "User Ressource")
public class AuthController {

  private final RegistrationService registrationService;
  private final AuthenticationService authenticationService;

  @PostMapping("/register")
  @Operation(summary = "Register a user")
  public ResponseEntity<String> register(@RequestBody RegistrationRequest request) {
    return ResponseEntity.ok(registrationService.register(request));
  }

  @PostMapping("/authenticate")
  @Operation(summary = "Authenticate a user")
  public ResponseEntity<Authentication> authenticate(@RequestBody AuthenticationRequest request) {
    return ResponseEntity.ok(authenticationService.authenticate(request));
  }
}
