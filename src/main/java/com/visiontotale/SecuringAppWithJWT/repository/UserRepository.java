package com.visiontotale.SecuringAppWithJWT.repository;

import com.visiontotale.SecuringAppWithJWT.model.entity.UserEntity;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/** Interface for user sql queries. */
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
  Optional<UserEntity> findByEmail(String email);

  @Query("SELECT u.profils FROM UserEntity u WHERE u.id = :userId")
  Set<String> findProfilsByUserId(@Param("userId") Integer userId);
}
