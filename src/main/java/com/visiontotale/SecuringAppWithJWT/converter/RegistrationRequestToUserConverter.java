package com.visiontotale.SecuringAppWithJWT.converter;

import com.visiontotale.SecuringAppWithJWT.enums.UserAttribute;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.authentication.User;
import com.visiontotale.SecuringAppWithJWT.model.dto.RegistrationRequest;
import java.util.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Class that is user to convert the RegistrationRequest to a Tokenizable User. While doing that, it
 * sets the claims that will appear in the token after registration.
 */
public class RegistrationRequestToUserConverter
    implements Converter<RegistrationRequest, TokenizableUser> {

  public RegistrationRequestToUserConverter() {}

  @Override
  public TokenizableUser convert(RegistrationRequest request) {

    Map<String, Object> claims = new HashMap<>();

    claims.put(UserAttribute.FIRST_NAME.name(), request.getFirstName());
    claims.put(UserAttribute.LAST_NAME.name(), request.getLastName());
    claims.put(
        UserAttribute.FULL_NAME.name(), request.getFirstName() + " " + request.getLastName());
    claims.put(UserAttribute.EMAIL.name(), request.getEmail());
    claims.put(UserAttribute.PROFILS.name(), request.getProfils());

    Set<String> profils = new HashSet<>(request.getProfils());

    return User.builder().claims(claims).authorities(getAuthorities(profils)).build();
  }

  private Collection<? extends GrantedAuthority> getAuthorities(Set<String> profils) {
    return profils.stream().map(SimpleGrantedAuthority::new).toList();
  }
}
