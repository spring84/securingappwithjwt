package com.visiontotale.SecuringAppWithJWT.converter;

/**
 * Generic conversion interface.
 *
 * @param <S> Source
 * @param <D> Destination
 */
public interface Converter<S, D> {

  /**
   * Convert from object S to object D.
   *
   * @param source the source object.
   * @return the converted object
   */
  D convert(S source);
}
