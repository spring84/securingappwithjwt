package com.visiontotale.SecuringAppWithJWT.converter;

import com.visiontotale.SecuringAppWithJWT.enums.UserAttribute;
import com.visiontotale.SecuringAppWithJWT.model.authentication.TokenizableUser;
import com.visiontotale.SecuringAppWithJWT.model.authentication.User;
import com.visiontotale.SecuringAppWithJWT.service.jwt.JwtService;
import io.jsonwebtoken.Claims;
import java.util.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class JwtToUserConverter implements Converter<String, TokenizableUser> {
  private final JwtService jwtService;

  public JwtToUserConverter(final JwtService jwtService) {
    this.jwtService = jwtService;
  }

  @Override
  public TokenizableUser convert(String jwt) {

    Claims claimsInToken = jwtService.extractAllClaims(jwt);

    Map<String, Object> claims =
        Map.of(
            UserAttribute.FIRST_NAME.name(),
            claimsInToken.get(UserAttribute.FIRST_NAME.name(), String.class),
            UserAttribute.LAST_NAME.name(),
            claimsInToken.get(UserAttribute.LAST_NAME.name(), String.class),
            UserAttribute.FULL_NAME.name(),
            claimsInToken.get(UserAttribute.FULL_NAME.name(), String.class),
            UserAttribute.EMAIL.name(),
            claimsInToken.get(UserAttribute.EMAIL.name(), String.class),
            UserAttribute.PROFILS.name(),
            claimsInToken.get(UserAttribute.PROFILS.name(), String.class));

    Set<String> profils = Set.of(claimsInToken.get(UserAttribute.PROFILS.name(), String.class));

    return User.builder().claims(claims).authorities(getAuthorities(profils)).build();
  }

  private Collection<? extends GrantedAuthority> getAuthorities(Set<String> profils) {
    return profils.stream().map(SimpleGrantedAuthority::new).toList();
  }
}
