package com.visiontotale.SecuringAppWithJWT.enums;

import lombok.Getter;

/** Error Messages. */
@Getter
public enum ErrorMessage {
  BAD_CREDENTIALS("Authentication failed: Invalid credentials."),
  USER_NOT_FOUND("User not found");

  private final String message;

  ErrorMessage(String message) {
    this.message = message;
  }
}
