package com.visiontotale.SecuringAppWithJWT.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/** Enum that holds the attributes that will be used to store the claims in a token */
@Getter
@AllArgsConstructor
public enum UserAttribute {
  FIRST_NAME("firstName"),
  LAST_NAME("lastName"),
  EMAIL("sub"),
  FULL_NAME("full name"),
  PROFILS("profils");

  private String label;
}
