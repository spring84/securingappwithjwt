package com.visiontotale.SecuringAppWithJWT.enums;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/** Enumeration of Error Codes according to Exceptions reported. */
@Getter
@AllArgsConstructor
public enum InterModuleError {
  FORMAT_INVALIDE("VT_0001", HttpStatus.BAD_REQUEST),
  ERREUR_VALIDATION("VT_0002", HttpStatus.BAD_REQUEST),
  ERREUR_TECHNIQUE("VT_0003", HttpStatus.INTERNAL_SERVER_ERROR),
  ERREUR_INCONNUE("VT_0004", HttpStatus.INTERNAL_SERVER_ERROR),
  ERREUR_AUTHENT("VT_0005", HttpStatus.BAD_REQUEST),
  ERREUR_AUTORISATION("VT_0006", HttpStatus.UNAUTHORIZED),
  AUCUN_RESULTAT("VT_0007", HttpStatus.NOT_FOUND),
  CONFLIT_VERSION("VT_0008", HttpStatus.CONFLICT),
  ERREUR_INTEGRITE_DONNEE("VT_0009", HttpStatus.CONFLICT);

  private String code;
  private HttpStatus status;

  /**
   * Method that determines if the reported error is one of the errors processed.
   *
   * @param code the error code returned by any given Vision Totale client module.
   * @return the presence or absence of the error in the enumeration.
   */
  public static boolean isPresent(String code) {
    return Arrays.stream(InterModuleError.values())
        .anyMatch(vtError -> vtError.getCode().equals(code));
  }

  /**
   * Method for recovering the value of the Enum corresponding to the Vision Totale error code.
   *
   * @param code the error code to be retrieved.
   * @return the corresponding enum value.
   */
  InterModuleError getVTErrorByCode(String code) {
    return Arrays.stream(InterModuleError.values())
        .filter(vtError -> vtError.getCode().equals(code))
        .findFirst()
        .orElse(InterModuleError.ERREUR_TECHNIQUE);
  }

  /**
   * Method to retrieve the value of the Enum corresponding to the HTTP Status code.
   *
   * @param status the http status code.
   * @return the corresponding enum value.
   */
  InterModuleError getVTErrorByStatus(HttpStatus status) {
    return Arrays.stream(InterModuleError.values())
        .filter(vtError -> vtError.getStatus() == status)
        .findFirst()
        .orElse(InterModuleError.ERREUR_TECHNIQUE);
  }
}
