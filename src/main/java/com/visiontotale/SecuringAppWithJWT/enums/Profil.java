package com.visiontotale.SecuringAppWithJWT.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/** Enum for all possible profiles of a user. */
@Getter
@AllArgsConstructor
public enum Profil {
  VISITOR("Visitor"),
  ADMIN("Admin"),
  SUPER_ADMIN("Super admin");

  private String label;
}
